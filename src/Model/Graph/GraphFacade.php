<?php

declare(strict_types=1);

namespace App\Model\Graph;

class GraphFacade
{
    /**
     * @var \App\Model\Graph\GraphRepository
     */
    private $graphRepository;

    /**
     * @param \App\Model\Graph\GraphRepository $graphRepository
     */
    public function __construct(GraphRepository $graphRepository)
    {
        $this->graphRepository = $graphRepository;
    }

    /**
     * @return \App\Model\Graph\Graph[]
     */
    public function findAll(): array
    {
        return $this->graphRepository->findAll();
    }
}
