<?php

namespace App\Model\Graph;

use App\Model\Metric\Metric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Graph\GraphRepository")
 */
class Graph
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $chartName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $chartType;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Metric\Metric", mappedBy="graph")
     */
    private $metrics;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $stacked;

    public function __construct()
    {
        $this->metrics = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getChartName(): ?string
    {
        return $this->chartName;
    }

    /**
     * @param string $chartName
     * @return $this
     */
    public function setChartName(string $chartName): self
    {
        $this->chartName = $chartName;

        return $this;
    }

    /**
     * @return Collection|Metric[]
     */
    public function getMetrics(): Collection
    {
        return $this->metrics;
    }

    /**
     * @param \App\Model\Metric\Metric $metric
     * @return $this
     */
    public function addMetric(Metric $metric): self
    {
        if (!$this->metrics->contains($metric)) {
            $this->metrics[] = $metric;
            $metric->setGraph($this);
        }

        return $this;
    }

    /**
     * @param \App\Model\Metric\Metric $metric
     * @return $this
     */
    public function removeMetric(Metric $metric): self
    {
        if ($this->metrics->contains($metric)) {
            $this->metrics->removeElement($metric);
            // set the owning side to null (unless already changed)
            if ($metric->getGraph() === $this) {
                $metric->setGraph(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getChartType()
    {
        return $this->chartType;
    }

    /**
     * @param string $chartType
     */
    public function setChartType(string $chartType): void
    {
        $this->chartType = $chartType;
    }

    /**
     * @return bool
     */
    public function isStacked(): bool
    {
        return $this->stacked;
    }

    /**
     * @param bool $stacked
     */
    public function setStacked(bool $stacked): void
    {
        $this->stacked = $stacked;
    }
}
