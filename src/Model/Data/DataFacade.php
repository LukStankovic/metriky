<?php

namespace App\Model\Data;

class DataFacade
{
    /**
     * @var \App\Model\Data\DataRepository
     */
    private $dataRepository;

    /**
     * @param \App\Model\Data\DataRepository $dataRepository
     */
    public function __construct(DataRepository $dataRepository)
    {
        $this->dataRepository = $dataRepository;
    }

    /**
     * @param \App\Model\Data\Data $data
     */
    public function save(Data $data)
    {
        $this->dataRepository->save($data);
    }

    /**
     * @param \App\Model\Data\Data[] $allData
     */
    public function saveAll(array $allData)
    {
        foreach ($allData as $data) {
            $this->dataRepository->save($data);
        }
    }
}
