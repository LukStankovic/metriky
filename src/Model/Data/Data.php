<?php

namespace App\Model\Data;

use App\Model\Data\Collection\DataCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Data\DataRepository")
 */
class Data
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Data\Collection\DataCollection", inversedBy="data")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dataCollection;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return \App\Model\Data\Collection\DataCollection|null
     */
    public function getDataCollection(): ?DataCollection
    {
        return $this->dataCollection;
    }

    /**
     * @param \App\Model\Data\Collection\DataCollection|null $dataCollection
     * @return $this
     */
    public function setDataCollection(?DataCollection $dataCollection): self
    {
        $this->dataCollection = $dataCollection;

        return $this;
    }
}
