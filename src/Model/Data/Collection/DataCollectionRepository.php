<?php

namespace App\Model\Data\Collection;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DataCollection|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataCollection|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataCollection[]    findAll()
 * @method DataCollection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataCollectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataCollection::class);
    }

    /**
     * @param \App\Model\Data\Collection\DataCollection $dataCollection
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(DataCollection $dataCollection)
    {
        $dataCollectionData = $dataCollection->getData();
        foreach ($dataCollectionData as $data) {
            $this->_em->persist($data);
        }
        $this->_em->persist($dataCollection);
        $this->_em->persist($dataCollection->getMetric());

        $this->_em->flush();
    }
}
