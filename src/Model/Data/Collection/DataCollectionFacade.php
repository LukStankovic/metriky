<?php

namespace App\Model\Data\Collection;

class DataCollectionFacade
{
    /**
     * @var \App\Model\Data\Collection\DataCollectionRepository
     */
    private $dataCollectionRepository;

    /**
     * @param \App\Model\Data\Collection\DataCollectionRepository $dataCollectionRepository
     */
    public function __construct(DataCollectionRepository $dataCollectionRepository)
    {
        $this->dataCollectionRepository = $dataCollectionRepository;
    }

    /**
     * @param \App\Model\Data\Collection\DataCollection $dataCollection
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(DataCollection $dataCollection)
    {
        $this->dataCollectionRepository->save($dataCollection);
    }

    /**
     * @param array $dataCollections
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveAll(array $dataCollections)
    {
        foreach ($dataCollections as $dataCollection) {
            $this->save($dataCollection);
        }
    }

    /**
     * @param \App\Model\Data\Collection\DataCollection[] $dataCollections
     * @return int
     */
    public function countDataInDataCollections(array $dataCollections): int
    {
        $dataCount = 0;
        foreach ($dataCollections as $dataCollection) {
            $dataCount += count($dataCollection->getData());
        }

        return $dataCount;
    }
}
