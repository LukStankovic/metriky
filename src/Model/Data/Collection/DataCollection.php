<?php

namespace App\Model\Data\Collection;

use App\Model\Data\Data;
use App\Model\Metric\Metric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Data\Collection\DataCollectionRepository")
 */
class DataCollection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Data\Data", mappedBy="dataCollection")
     */
    private $data;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Metric\Metric", inversedBy="dataCollections")
     * @ORM\JoinColumn(nullable=false)
     */
    private $metric;

    public function __construct()
    {
        $this->data = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    /**
     * @param \DateTimeInterface $datetime
     * @return $this
     */
    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|\App\Model\Data\Data[]
     */
    public function getData(): Collection
    {
        return $this->data;
    }

    /**
     * @param \App\Model\Data\Data $data
     * @return $this
     */
    public function addData(Data $data): self
    {
        if (!$this->data->contains($data)) {
            $this->data[] = $data;
            $data->setDataCollection($this);
        }

        return $this;
    }

    /**
     * @param \App\Model\Data\Data $data
     * @return $this
     */
    public function removeData(Data $data): self
    {
        if ($this->data->contains($data)) {
            $this->data->removeElement($data);
            // set the owning side to null (unless already changed)
            if ($data->getDataCollection() === $this) {
                $data->setDataCollection(null);
            }
        }

        return $this;
    }

    /**
     * @return \App\Model\Metric\Metric|null
     */
    public function getMetric(): ?Metric
    {
        return $this->metric;
    }

    /**
     * @param \App\Model\Metric\Metric|null $metric
     * @return $this
     */
    public function setMetric(?Metric $metric): self
    {
        $this->metric = $metric;

        return $this;
    }
}
