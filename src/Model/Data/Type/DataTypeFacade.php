<?php

namespace App\Model\Data\Type;

class DataTypeFacade
{
    /**
     * @var \App\Model\Data\DataRepository
     */
    private $dataTypeRepository;

    /**
     * @param \App\Model\Data\Type\DataTypeRepository $dataTypeRepository
     */
    public function __construct(DataTypeRepository $dataTypeRepository)
    {
        $this->dataTypeRepository = $dataTypeRepository;
    }

    /**
     * @param \App\Model\Data\Type\DataType $dataType
     */
    public function save(DataType $dataType)
    {
        $this->dataTypeRepository->save($dataType);
    }

    /**
     * @param int $dataTypeId
     * @return \App\Model\Data\Type\DataType|null
     */
    public function getById(int $dataTypeId)
    {
        return $this->dataTypeRepository->getById($dataTypeId);
    }

    /**
     * @param \App\Model\Data\Type\DataType $dataType
     */
    public function remove(DataType $dataType)
    {
        $this->dataTypeRepository->remove($dataType);
    }

    /**
     * @param string $value
     * @return string[]
     */
    public function getBasicDataTypeNameByValue(string $value): array
    {
        $dataTypes = [
            'int' => 'Integer - celé číslo',
            'double' => 'Double - desetinné číslo',
            'varchar' => 'String (max 255 znaků)',
            'date' => 'Datum',
            'datetime' => 'Datum s časem',
        ];

        return $dataTypes[$value] ?? [];
    }
}
