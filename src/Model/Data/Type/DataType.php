<?php

namespace App\Model\Data\Type;

use App\Model\Metric\Metric;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Data\Type\DataTypeRepository")
 */
class DataType
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var \App\Model\Metric\Metric
     *
     * @ORM\ManyToOne(targetEntity="App\Model\Metric\Metric", inversedBy="dataTypes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $metric;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \App\Model\Metric\Metric|null
     */
    public function getMetric(): ?Metric
    {
        return $this->metric;
    }

    /**
     * @param \App\Model\Metric\Metric|null $metric
     * @return $this
     */
    public function setMetric(?Metric $metric): self
    {
        $this->metric = $metric;

        return $this;
    }
}
