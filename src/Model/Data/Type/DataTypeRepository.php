<?php

namespace App\Model\Data\Type;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DataType|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataType|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataType[]    findAll()
 * @method DataType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataType::class);
    }

    /**
     * @param \App\Model\Data\Type\DataType $dataType
     */
    public function save(DataType $dataType)
    {
        $this->_em->persist($dataType);
        $this->_em->flush();
    }

    /**
     * @param int $id
     * @return \App\Model\Data\Type\DataType|null
     */
    public function getById(int $id): ?DataType
    {
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * @param \App\Model\Data\Type\DataType $dataType
     */
    public function remove(DataType $dataType)
    {
        $this->_em->remove($dataType);
        $this->_em->flush();
    }
}
