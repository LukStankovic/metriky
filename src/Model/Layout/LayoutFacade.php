<?php

namespace App\Model\Layout;

use App\Model\User\User;
use Symfony\Component\Security\Core\User\UserInterface;

class LayoutFacade
{
    /**
     * @var \App\Model\Layout\LayoutRepository
     */
    private $layoutRepository;

    /**
     * @param \App\Model\Layout\LayoutRepository $layoutRepository
     */
    public function __construct(LayoutRepository $layoutRepository)
    {
        $this->layoutRepository = $layoutRepository;
    }

    /**
     * @param \App\Model\User\User $user
     * @return \App\Model\Layout\Layout[]
     */
    public function findByUser(User $user): array
    {
        return $this->layoutRepository->findByUser($user);
    }

    /**
     * @param \App\Model\User\User $user
     * @return \App\Model\Metric\Metric[]
     */
    public function findLayoutMetricsByUser(User $user): array
    {
        $layouts = $this->layoutRepository->findByUser($user);

        return $this->findLayoutMetricsForLayouts($layouts);
    }

    /**
     * @param array $layouts
     * @return \App\Model\Metric\Metric[]
     */
    public function findLayoutMetricsForLayouts(array $layouts): array
    {
        $metrics = [];
        foreach ($layouts as $layout) {
            $metrics[$layout->getMetric()->getId()] = $layout->getMetric();
        }

        return $metrics;
    }

    public function save(Layout $layout): void
    {
        $this->layoutRepository->save($layout);
    }

    /**
     * @param \App\Model\Layout\Layout[] $layouts
     * @return int
     */
    public function getNextPosition(array $layouts): int
    {
        $highestPosition = 0;

        foreach ($layouts as $layout) {
            if ($layout->getPosition() > $highestPosition) {
                $highestPosition = $layout->getPosition();
            }
        }

        return $highestPosition + 1;
    }

    /**
     * @param array $layouts
     */
    public function saveAll(array $layouts): void
    {
        $this->layoutRepository->saveAll($layouts);
    }

    public function removeAllUserLayouts(User $user)
    {
        $this->layoutRepository->removeAllUserLayouts($user);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAll(): void
    {
        $this->layoutRepository->updateAll();
    }

    /**
     * @param int $layoutId
     * @param \Symfony\Component\Security\Core\User\UserInterface|null $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeByLayoutIdAndUser(
        int $layoutId,
        ?UserInterface $user
    ): void {
        $this->layoutRepository->removeByLayoutIdAndUser($layoutId, $user);
    }

    /**
     * @param int $layoutId
     * @param \Symfony\Component\Security\Core\User\UserInterface|null $user
     * @return \App\Model\Layout\Layout|null
     */
    public function getByLayoutId(int $layoutId, ?UserInterface $user)
    {
        return $this->layoutRepository->getByLayoutId($layoutId, $user);
    }
}
