<?php

namespace App\Model\Layout;

use App\Model\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Layout|null find($id, $lockMode = null, $lockVersion = null)
 * @method Layout|null findOneBy(array $criteria, array $orderBy = null)
 * @method Layout[]    findAll()
 * @method Layout[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LayoutRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Layout::class);
    }

    /**
     * @param \App\Model\User\User $user
     * @return \App\Model\Layout\Layout[]
     */
    public function findByUser(User $user): array
    {
        return $this->findBy(['user' => $user], ['position' => 'ASC']);
    }

    /**
     * @param \App\Model\Layout\Layout $layout
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Layout $layout): void
    {
        $this->_em->persist($layout);
        $this->_em->flush();
    }

    /**
     * @param \App\Model\Layout\Layout[] $layouts
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveAll(array $layouts): void
    {
        foreach ($layouts as $layout) {
            $this->_em->persist($layout);
        }
        $this->_em->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAll(): void
    {
        $this->_em->flush();
    }

    /**
     * @param \App\Model\User\User $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeAllUserLayouts(User $user)
    {
        $layouts = $this->findByUser($user);

        foreach ($layouts as $layout) {
            $this->_em->remove($layout);
        }

        $this->_em->flush();
    }

    /**
     * @param int $layoutId
     * @param \Symfony\Component\Security\Core\User\UserInterface|null $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeByLayoutIdAndUser(int $layoutId, ?UserInterface $user)
    {
        $layout = $this->getByLayoutId($layoutId, $user);

        if ($layout !== null) {
            $this->_em->remove($layout);
            $this->_em->flush();
        }
    }

    /**
     * @param int $layoutId
     * @param \Symfony\Component\Security\Core\User\UserInterface|null $user
     * @return \App\Model\Layout\Layout|null
     */
    public function getByLayoutId(int $layoutId, ?UserInterface $user)
    {
        return $this->findOneBy(['user' => $user, 'id' => $layoutId]);
    }
}
