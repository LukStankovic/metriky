<?php

namespace App\Model\Import;

use App\Model\Data\Collection\DataCollection;
use App\Model\Data\Collection\DataCollectionFacade;
use App\Model\Data\Data;
use App\Model\Metric\Metric;
use App\Model\Metric\MetricFacade;
use DateTime;
use League\Csv\Reader;

class CsvImportFacade
{
    public const METRIC_ID_COLUMN_INDEX = 0;

    public const DATE_COLUMN_INDEX = 1;

    public const NAME_COLUMN_INDEX = 2;

    public const VALUE_COLUMN_INDEX = 3;

    public const NAME_COLUMN_TYPE = 'name';

    public const VALUE_COLUMN_TYPE = 'value';

    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;

    /**
     * @var \App\Model\Data\Collection\DataCollectionFacade
     */
    private $dataCollectionFacade;

    /**
     * @param \App\Model\Data\Collection\DataCollectionFacade $dataCollectionFacade
     * @param \App\Model\Metric\MetricFacade $metricFacade
     */
    public function __construct(DataCollectionFacade $dataCollectionFacade, MetricFacade $metricFacade)
    {
        $this->dataCollectionFacade = $dataCollectionFacade;
        $this->metricFacade = $metricFacade;
    }

    /**
     * @param string $filePath
     * @return \App\Model\Data\Collection\DataCollection[]|array
     * @throws \League\Csv\Exception
     */
    public function parseAndSaveDataCollectionsFromCsv(string $filePath): array
    {
        $csv = Reader::createFromPath($filePath);
        $csv->setDelimiter(';');

        $dataCollections = [];
        foreach ($csv as $record) {
            $metric = $this->metricFacade->getById((int) $record[CsvImportFacade::METRIC_ID_COLUMN_INDEX]);
            if ($metric === null) {
                continue;
            }
            $dataCollections[] = $this->getDataCollectionFromRecord($record, $metric);
        }

        $this->dataCollectionFacade->saveAll($dataCollections);

        return $dataCollections;
    }

    /**
     * @param array $record
     * @param \App\Model\Metric\Metric $metric
     * @return \App\Model\Data\Collection\DataCollection
     */
    private function getDataCollectionFromRecord(array $record, Metric $metric): DataCollection
    {
        $dataCollection = new DataCollection();
        $dataCollection->setMetric($metric);

        /** @var \App\Model\Data\Data[] $dataForDataCollections */
        $dataForDataCollections = [];
        $i = 0;
        foreach ($record as $index => $column) {
            if ($index === CsvImportFacade::DATE_COLUMN_INDEX) {
                $date = DateTime::createFromFormat('j.n.Y', $column);
                $dataCollection->setDatetime($date);
            } elseif ($index > CsvImportFacade::DATE_COLUMN_INDEX) {
                $columnType = $index % 2 === 0 ? CsvImportFacade::NAME_COLUMN_TYPE : CsvImportFacade::VALUE_COLUMN_TYPE;

                if ($columnType === CsvImportFacade::NAME_COLUMN_TYPE) {
                    $data = new Data();
                    $data->setName($column);
                    $dataForDataCollections[$i] = $data;
                } elseif ($columnType === CsvImportFacade::VALUE_COLUMN_TYPE) {
                    $dataForDataCollections[$i]->setValue($column);
                    $dataCollection->addData($dataForDataCollections[$i]);
                    $i++;
                }
            }
        }

        return $dataCollection;
    }
}
