<?php

namespace App\Model\Metric;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Metric|null find($id, $lockMode = null, $lockVersion = null)
 * @method Metric|null findOneBy(array $criteria, array $orderBy = null)
 * @method Metric[]    findAll()
 * @method Metric[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MetricRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Metric::class);
    }

    /**
     * @param \App\Model\Metric\Metric $metric
     */
    public function save(Metric $metric)
    {
        $this->_em->persist($metric);
        $this->_em->flush();
    }

    /**
     * @param int $id
     * @return \App\Model\Metric\Metric|null
     */
    public function getById(int $id): ?Metric
    {
        return $this->findOneBy(['id' => $id]);
    }
}
