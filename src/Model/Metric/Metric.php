<?php

namespace App\Model\Metric;

use App\Model\Data\Collection\DataCollection;
use App\Model\Data\Type\DataType;
use App\Model\Graph\Graph;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Metric\MetricRepository")
 */
class Metric
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Graph\Graph", inversedBy="metrics")
     */
    private $graph;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Data\Collection\DataCollection", mappedBy="metric")
     */
    private $dataCollections;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Data\Type\DataType", mappedBy="metric")
     */
    private $dataTypes;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Layout\Layout", mappedBy="metric")
     */
    private $layouts;

    /**
     * @param int|null $id
     */
    public function __construct(?int $id = null)
    {
        if ($id !== null) {
            $this->id = $id;
        }
        $this->dataCollections = new ArrayCollection();
        $this->dataTypes = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \App\Model\Graph\Graph|null
     */
    public function getGraph(): ?Graph
    {
        return $this->graph;
    }

    /**
     * @param \App\Model\Graph\Graph|null $graph
     * @return $this
     */
    public function setGraph(?Graph $graph): self
    {
        $this->graph = $graph;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|\App\Model\Data\Collection\DataCollection[]
     */
    public function getDataCollections()
    {
        return $this->dataCollections;
    }

    /**
     * @param \App\Model\Data\Collection\DataCollection $dataCollection
     * @return $this
     */
    public function addDataCollection(DataCollection $dataCollection): self
    {
        if (!$this->dataCollections->contains($dataCollection)) {
            $this->dataCollections[] = $dataCollection;
            $dataCollection->setMetric($this);
        }

        return $this;
    }

    /**
     * @param \App\Model\Data\Collection\DataCollection $dataCollection
     * @return $this
     */
    public function removeDataCollection(DataCollection $dataCollection): self
    {
        if ($this->dataCollections->contains($dataCollection)) {
            $this->dataCollections->removeElement($dataCollection);
            // set the owning side to null (unless already changed)
            if ($dataCollection->getMetric() === $this) {
                $dataCollection->setMetric(null);
            }
        }

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection| \App\Model\Data\Type\DataType[]
     */
    public function getDataTypes(): Collection
    {
        return $this->dataTypes;
    }

    /**
     * @param \App\Model\Data\Type\DataType $dataType
     * @return $this
     */
    public function addDataType(DataType $dataType): self
    {
        if (!$this->dataTypes->contains($dataType)) {
            $this->dataTypes[] = $dataType;
            $dataType->setMetric($this);
        }

        return $this;
    }

    /**
     * @param \App\Model\Data\Type\DataType $dataType
     * @return $this
     */
    public function removeDataType(DataType $dataType): self
    {
        if ($this->dataTypes->contains($dataType)) {
            $this->dataTypes->removeElement($dataType);
            // set the owning side to null (unless already changed)
            if ($dataType->getMetric() === $this) {
                $dataType->setMetric(null);
            }
        }

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLayouts()
    {
        return $this->layouts;
    }

    /**
     * @param \App\Model\Layout\Layout[] $layouts
     */
    public function setLayouts(array $layouts): void
    {
        $this->layouts = $layouts;
    }
}
