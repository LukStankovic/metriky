<?php

namespace App\Model\Metric;

class MetricFacade
{
    /**
     * @var \App\Model\Metric\MetricRepository
     */
    private $metricRepository;

    /**
     * @param \App\Model\Metric\MetricRepository $metricRepository
     */
    public function __construct(MetricRepository $metricRepository)
    {
        $this->metricRepository = $metricRepository;
    }

    /**
     * @param int $id
     * @return \App\Model\Metric\Metric|null
     */
    public function getById(int $id): ?Metric
    {
        return $this->metricRepository->getById($id);
    }

    /**
     * @return \App\Model\Metric\Metric[]
     */
    public function findAll(): array
    {
        return $this->metricRepository->findAll();
    }

    /**
     * @param \App\Model\Metric\Metric $metric
     */
    public function save(Metric $metric)
    {
        $this->metricRepository->save($metric);
    }

    /**
     * @param \App\Model\Metric\Metric[] $metrics
     */
    public function saveAll(array $metrics)
    {
        foreach ($metrics as $metric) {
            $this->save($metric);
        }
    }

    /**
     * @param \App\Model\Metric\Metric $metric
     * @return array
     */
    public function getMetricDataForChart(Metric $metric): array
    {
        $dataCollections = $metric->getDataCollections();
        $dataTypes = $metric->getDataTypes();
        $metricDataForChart = [];

        foreach ($dataCollections as $key => $dataCollection) {
            $metricDataForChart[$key]['datetime'] = $dataCollection->getDatetime()->format('Y-m-d H:i:s');

            foreach ($dataTypes as $dataType) {
                foreach ($dataCollection->getData() as $data) {
                    if ($data->getName() === $dataType->getName()) {
                        $value = $data->getValue();
                        $type = $dataType->getType();

                        if (!in_array($type, ['date', 'datetime'])) {
                            settype($value, $type);
                        }

                        $metricDataForChart[$key][$dataType->getName()] = $value;
                    }
                }
            }
        }

        return $metricDataForChart;
    }

    public function getMetricDataForPieChart(Metric $metric)
    {
        $dataCollections = $metric->getDataCollections();
        $metricDataForPieChart = [];

        foreach ($dataCollections as $dataCollection) {
            foreach ($dataCollection->getData() as $data) {
                $valueData['name'] = $data->getName();
                $valueData['value'] = $data->getValue();

                $metricDataForPieChart[$dataCollection->getDatetime()->getTimestamp()][] = $valueData;
            }
        }

        return $metricDataForPieChart;
    }
}
