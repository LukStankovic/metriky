<?php

namespace App\Model\User;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserFacade
{
    /**
     * @var \App\Model\User\UserRepository
     */
    private $userRepository;

    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(
        UserRepository $userRepository,
        TokenStorageInterface $tokenStorage
    ) {
        $this->userRepository = $userRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return \App\Model\Layout\Layout[]|array
     */
    public function getLayoutsByLoggedUser(): array
    {
        $user = $this->userRepository->findById($this->getLoggedUser()->getId());

        return $user->getLayouts();
    }

    /**
     * @return \App\Model\User\User
     */
    public function getLoggedUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}
