<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Model\Data\Type\DataType;
use App\Model\Graph\Graph;
use App\Model\Metric\Metric;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MetricFixtures extends Fixture implements ContainerAwareInterface
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $this->resetAutoIncrements($manager);

        $this->barGraphNotStacked($manager);
        $this->barMetricStacked($manager);
        $this->pieMetric($manager);
        $this->lineMetric($manager);

        $manager->flush();
    }

    private function lineMetric(ObjectManager $manager)
    {
        $graph = new Graph();
        $graph->setChartName('bar');
        $graph->setName('Spojnicový graf');
        $graph->setChartType('xy_line');
        $graph->setStacked(false);
        $manager->persist($graph);

        $metric = new Metric();
        $metric->setName('Spojnicová metrika');
        $metric->setDescription('Toto je pouze testovací metrika');
        $metric->setGraph($graph);
        $manager->persist($metric);

        $dataType = new DataType();
        $dataType->setMetric($metric);
        $dataType->setType('int');
        $dataType->setName('Splněných');
        $dataType->setDescription('Počet splněných úkolů');
        $manager->persist($dataType);

        $dataType = new DataType();
        $dataType->setMetric($metric);
        $dataType->setType('int');
        $dataType->setName('Nesplněných');
        $dataType->setDescription('Počet nesplněných úkolů');
        $manager->persist($dataType);

        $dataType = new DataType();
        $dataType->setMetric($metric);
        $dataType->setType('int');
        $dataType->setName('Rozdělaných');
        $dataType->setDescription('Počet rozdělaných úkolů');
        $manager->persist($dataType);

        $manager->flush();
    }

    private function barGraphNotStacked(ObjectManager $manager)
    {
        $graph = new Graph();
        $graph->setChartName('bar');
        $graph->setName('Sloupcový graf');
        $graph->setChartType('xy_bar');
        $graph->setStacked(false);
        $manager->persist($graph);
        $manager->flush();
    }

    private function barMetricStacked(ObjectManager $manager)
    {
        $graph = new Graph();
        $graph->setChartName('bar');
        $graph->setName('Sloupcový graf - skládaný');
        $graph->setChartType('xy_bar');
        $graph->setStacked(true);
        $manager->persist($graph);

        $metric = new Metric();
        $metric->setName('Sloupcová metrika');
        $metric->setDescription('Toto je pouze testovací metrika');
        $metric->setGraph($graph);
        $manager->persist($metric);

        $dataType = new DataType();
        $dataType->setMetric($metric);
        $dataType->setType('int');
        $dataType->setName('Splněných');
        $dataType->setDescription('Počet splněných úkolů');
        $manager->persist($dataType);

        $dataType = new DataType();
        $dataType->setMetric($metric);
        $dataType->setType('int');
        $dataType->setName('Nesplněných');
        $dataType->setDescription('Počet nesplněných úkolů');
        $manager->persist($dataType);

        $dataType = new DataType();
        $dataType->setMetric($metric);
        $dataType->setType('int');
        $dataType->setName('Rozdělaných');
        $dataType->setDescription('Počet rozdělaných úkolů');
        $manager->persist($dataType);

        $manager->flush();
    }

    private function pieMetric(ObjectManager $manager)
    {
        $graph = new Graph();
        $graph->setChartName('pie');
        $graph->setChartType('pie');
        $graph->setName('Koláčový graf');
        $graph->setStacked(false);
        $manager->persist($graph);

        $metric = new Metric();
        $metric->setName('Koláčová metrika');
        $metric->setDescription('Toto je pouze testovací metrika');
        $metric->setGraph($graph);
        $manager->persist($metric);
        $manager->flush();
    }

    private function resetAutoIncrements(ObjectManager $manager)
    {

        $stmt = $this->container->get('doctrine.orm.entity_manager')->getConnection()->prepare("ALTER TABLE `data` AUTO_INCREMENT = 1");
        $stmt->execute();

        $stmt = $this->container->get('doctrine.orm.entity_manager')->getConnection()->prepare("ALTER TABLE `data_collection` AUTO_INCREMENT = 1");
        $stmt->execute();

        $stmt = $this->container->get('doctrine.orm.entity_manager')->getConnection()->prepare("ALTER TABLE `data_type` AUTO_INCREMENT = 1");
        $stmt->execute();

        $stmt = $this->container->get('doctrine.orm.entity_manager')->getConnection()->prepare("ALTER TABLE `graph` AUTO_INCREMENT = 1");
        $stmt->execute();

        $stmt = $this->container->get('doctrine.orm.entity_manager')->getConnection()->prepare("ALTER TABLE `layout` AUTO_INCREMENT = 1");
        $stmt->execute();

        $stmt = $this->container->get('doctrine.orm.entity_manager')->getConnection()->prepare("ALTER TABLE `metric` AUTO_INCREMENT = 1");
        $stmt->execute();

        $stmt = $this->container->get('doctrine.orm.entity_manager')->getConnection()->prepare("ALTER TABLE `user` AUTO_INCREMENT = 1");
        $stmt->execute();
    }

    /**
     * @inheritDoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
