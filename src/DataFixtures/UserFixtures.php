<?php

namespace App\DataFixtures;

use App\Model\User\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setEmail('admin@vsb.cz');
        $admin->setName('Admin');
        $admin->setSurname('Admin');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPassword($this->passwordEncoder->encodePassword($admin, 'admin1234'));
        $manager->persist($admin);

        $user = new User();
        $user->setEmail('test@vsb.cz');
        $user->setName('Test');
        $user->setSurname('Test');
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'user1234'));
        $manager->persist($user);

        $manager->flush();
    }
}
