<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Form\Front\NewMetricFormType;
use App\Model\Metric\Metric;
use App\Model\Metric\MetricFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MetricController extends AbstractController
{
    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;

    /**
     * @param \App\Model\Metric\MetricFacade $metricFacade
     */
    public function __construct(
        MetricFacade $metricFacade
    ) {
        $this->metricFacade = $metricFacade;
    }

    /**
     * @Route("/admin/metriky", name="admin_metrics_list")
     */
    public function metricsList()
    {
        $metrics = $this->metricFacade->findAll();

        return $this->render('Admin/metric/metrics_list.html.twig', [
            'metrics' => $metrics,
        ]);
    }

    /**
     * @Route("/admin/metrika/upravit/{metricId}", name="admin_edit_metric")
     * @param Request $request
     * @param int $metricId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, int $metricId)
    {
        $metric = $this->metricFacade->getById($metricId);
        $form = $this->createForm(NewMetricFormType::class, $metric);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $metric->setName($form->get('name')->getData());
            $metric->setGraph($form->get('graph')->getData());
            $metric->setDescription($form->get('description')->getData());

            $this->metricFacade->save($metric);
        }

        return $this->render('Admin/metric/edit.html.twig', [
                'newMetricForm' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/admin/nova-metrika", name="admin_new_metric")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newMetric(Request $request)
    {
        $metric = new Metric();
        $form = $this->createForm(NewMetricFormType::class, $metric);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $metric->setName($form->get('name')->getData());
            $metric->setGraph($form->get('graph')->getData());
            $metric->setDescription($form->get('description')->getData());

            $this->metricFacade->save($metric);

            return $this->render('Admin/metric/edit.html.twig', [
                    'metric' => $metric,
                ]
            );
        }

        return $this->render('Admin/metric/new.html.twig', [
                'newMetricForm' => $form->createView(),
            ]
        );
    }
}
