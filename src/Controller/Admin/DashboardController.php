<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\Metric\MetricFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;

    /**
     * @param \App\Model\Metric\MetricFacade $metricFacade
     */
    public function __construct(
        MetricFacade $metricFacade
    ) {
        $this->metricFacade = $metricFacade;
    }

    /**
     * @Route("/admin/", name="admin_dashboard")
     */
    public function index()
    {
        $metrics = $this->metricFacade->findAll();

        return $this->render('Admin/dashboard/index.html.twig', [
            'metrics' => $metrics,
        ]);
    }
}
