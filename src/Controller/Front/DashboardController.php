<?php

namespace App\Controller\Front;

use App\Form\Front\AddMetricToLayoutFormType;
use App\Model\Layout\Layout;
use App\Model\Layout\LayoutFacade;
use App\Model\Metric\MetricFacade;
use App\Model\User\UserFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;

class DashboardController extends AbstractController
{
    private $logger;

    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;

    /**
     * @var \App\Model\User\UserFacade
     */
    private $userFacade;
    /**
     * @var \App\Model\Layout\LayoutFacade
     */
    private $layoutFacade;

    /**
     * @param \App\Model\Metric\MetricFacade $metricFacade
     * @param \Psr\Log\LoggerInterface $logger
     * @param \App\Model\User\UserFacade $userFacade
     * @param \App\Model\Layout\LayoutFacade $layoutFacade
     */
    public function __construct(
        MetricFacade $metricFacade,
        LoggerInterface $logger,
        UserFacade $userFacade,
        LayoutFacade $layoutFacade
    ) {
        $this->logger = $logger;
        $this->metricFacade = $metricFacade;
        $this->userFacade = $userFacade;
        $this->layoutFacade = $layoutFacade;
    }

    /**
     * @Route("/", name="dashboard")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $metrics = $this->metricFacade->findAll();
        $layouts = $this->layoutFacade->findByUser($this->userFacade->getLoggedUser());
        $availableMetrics = $this->getAvailableMetricForLayout($metrics, $layouts);

        $layoutForm = $this->createForm(AddMetricToLayoutFormType::class, $availableMetrics);
        $layoutForm->handleRequest($request);

        if ($layoutForm->isSubmitted() && $layoutForm->isValid()) {
            $newLayout = new Layout();
            $newLayout->setMetric($layoutForm['metric']->getData());
            $newLayout->setUser($this->userFacade->getLoggedUser());
            $newLayout->setPosition($this->layoutFacade->getNextPosition($layouts));
            $this->layoutFacade->save($newLayout);

            $layouts = $this->layoutFacade->findByUser($this->userFacade->getLoggedUser());
            $availableMetrics = $this->getAvailableMetricForLayout($metrics, $layouts);
            $layoutForm = null;
            $layoutForm = $this->createForm(AddMetricToLayoutFormType::class, $availableMetrics);
            $this->redirectToRoute('dashboard');
        }

        return $this->render('Front/dashboard/index.html.twig', [
            'metrics' => $metrics,
            'layouts' => $layouts,
            'availableMetrics' => $availableMetrics,
            'layoutForm' => $layoutForm->createView(),
        ]);
    }

    /**
     * @param \App\Model\Metric\Metric[] $metrics
     * @param \App\Model\Layout\Layout[] $layouts
     * @return array
     */
    private function getAvailableMetricForLayout(array $metrics, array $layouts): array
    {
        $availableMetrics = [];
        $layoutMetrics = $this->layoutFacade->findLayoutMetricsForLayouts($layouts);
        $layoutMetricIds = array_keys($layoutMetrics);

        foreach ($metrics as $metric) {
            if (!in_array($metric->getId(), $layoutMetricIds)) {
                $availableMetrics[] = $metric;
            }
        }

        return $availableMetrics;
    }

    /*
    /**
     * @Route("/", name="dashboard")
     */
    /*
    public function showDashboard() {
        $metrics[] = $this->metricFacade->findAll();
        $delka = sizeof($metrics);
        $jmeno = "Tonda";
        $cisla = [];
        for($i = 0; $i < 5; $i++) {
            $cisla[$i] = $i;
        }

        $dataForChart = [];
        $results = [
            'delka' => $delka,
            'jmeno' => $jmeno,
            'cisla' => $cisla,

        ];



        foreach ($metrics as $m) {
            $this->logger->info('metrics count '.$delka);
        }


        foreach ($metrics as $metric) {
            $dataTypesForChart = [];
            $dataForChart[] = $this->metricFacade->getMetricForChart($metric);
            foreach ($metric->getDataTypes() as $dataType) {
                $dataTypesForChart[] = $dataType->getName();
            }
            $results += [
                'metric' => $metric,
                'chartValues' => $dataForChart,
                'chartTypes' => $dataTypesForChart,
            ];
        }

        return $this->render('dashboard/index.html.twig', $results);
        //return $this->render('dashboard/index.html.twig', ['delka' => $delka]);
    }
    */
}
