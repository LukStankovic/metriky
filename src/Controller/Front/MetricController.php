<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Form\Front\NewMetricFormType;
use App\Model\Data\Type\DataTypeFacade;
use App\Model\Metric\Metric;
use App\Model\Metric\MetricFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MetricController extends AbstractController
{
    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;
    /**
     * @var \App\Model\Data\Type\DataTypeFacade
     */
    private $dataTypeFacade;

    /**
     * @param \App\Model\Metric\MetricFacade $metricFacade
     * @param \App\Model\Data\Type\DataTypeFacade $dataTypeFacade
     */
    public function __construct(
        MetricFacade $metricFacade,
        DataTypeFacade $dataTypeFacade
    ) {
        $this->metricFacade = $metricFacade;
        $this->dataTypeFacade = $dataTypeFacade;
    }

    /**
     * @param int $metricId
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("metrika/{metricId}", name="metric_detail")
     */
    public function metricDetail(int $metricId)
    {
        $metric = $this->metricFacade->getById($metricId);
        if (in_array($metric->getGraph()->getChartType(), ['xy_bar', 'xy_line'])) {
            $dataForChart = $this->metricFacade->getMetricDataForChart($metric);
        } else {
            $dataForChart = $this->metricFacade->getMetricDataForPieChart($metric);
        }

        $dataTypesForChart = [];
        foreach ($metric->getDataTypes() as $dataType) {
            $dataTypesForChart[] = $dataType->getName();
        }

        return $this->render('Front/metric/detail.html.twig', [
                'metric' => $metric,
                'chartValues' => $dataForChart,
                'chartTypes' => $dataTypesForChart,
            ]
        );
    }

    /**
     * @Route("/nova-metrika", name="new_metric")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newMetric(Request $request)
    {
        $metric = new Metric();
        $form = $this->createForm(NewMetricFormType::class, $metric);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $metric->setName($form->get('name')->getData());
            $metric->setGraph($form->get('graph')->getData());
            $metric->setDescription($form->get('description')->getData());

            $this->metricFacade->save($metric);
        }

        return $this->render('Front/new_metric/new.html.twig', [
                'newMetricForm' => $form->createView(),
            ]
        );
    }
}
