<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Model\Metric\MetricFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class NavigationController extends AbstractController
{
    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;

    /**
     * @param \App\Model\Metric\MetricFacade $metricFacade
     */
    public function __construct(MetricFacade $metricFacade)
    {
        $this->metricFacade = $metricFacade;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function navigation(Request $request)
    {
        $metrics = $this->metricFacade->findAll();

        return $this->render('Layout/navigation.html.twig', [
                'metrics' => $metrics,
            ]
        );
    }
}
