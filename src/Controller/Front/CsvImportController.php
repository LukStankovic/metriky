<?php

namespace App\Controller\Front;

use App\Form\Front\CsvImportFormType;
use App\Model\Data\Collection\DataCollectionFacade;
use App\Model\Data\DataFacade;
use App\Model\Import\CsvImportFacade;
use App\Model\Metric\MetricFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;

class CsvImportController extends AbstractController
{
    /**
     * @var KernelInterface
     */
    private $appKernel;

    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;

    /**
     * @var \App\Model\Data\DataFacade
     */
    private $dataFacade;

    /**
     * @var \App\Model\Import\CsvImportFacade
     */
    private $csvImportFacade;

    /**
     * @var int
     */
    private $createdData = 0;

    /**
     * @var int
     */
    private $createdDataCollections = 0;

    /**
     * @var \App\Model\Data\Collection\DataCollectionFacade
     */
    private $dataCollectionFacade;

    /**
     * @param \Symfony\Component\HttpKernel\KernelInterface $appKernel
     * @param \App\Model\Metric\MetricFacade $metricFacade
     * @param \App\Model\Data\DataFacade $dataFacade
     * @param \App\Model\Import\CsvImportFacade $csvImportFacade
     * @param \App\Model\Data\Collection\DataCollectionFacade $dataCollectionFacade
     */
    public function __construct(
        KernelInterface $appKernel,
        MetricFacade $metricFacade,
        DataFacade $dataFacade,
        CsvImportFacade $csvImportFacade,
        DataCollectionFacade $dataCollectionFacade
    ) {
        $this->appKernel = $appKernel;
        $this->metricFacade = $metricFacade;
        $this->dataFacade = $dataFacade;
        $this->csvImportFacade = $csvImportFacade;
        $this->dataCollectionFacade = $dataCollectionFacade;
    }

    /**
     * @Route("/import", name="csv_import")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function csvImport(Request $request)
    {
        $isDataSaved = false;
        $form = $this->createForm(CsvImportFormType::class);
        $form->handleRequest($request);
        $dataCollections = [];
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedCsvFile */
            $uploadedCsvFile = $form['csv']->getData();

            if ($uploadedCsvFile) {
                $originalFilename = pathinfo($uploadedCsvFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate(
                    'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename
                );
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $uploadedCsvFile->guessExtension();

                try {
                    $uploadedCsvFile->move(
                        $this->getParameter('csv_upload_dir'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }

                $newFilePath = $this->getParameter('csv_upload_dir') . $newFilename;
                $dataCollections = $this->csvImportFacade->parseAndSaveDataCollectionsFromCsv($newFilePath);
                $isDataSaved = !empty($dataCollections);
            }
        }

        return $this->render('Front/import/csv_import.html.twig', [
                'importForm' => $form->createView(),
                'createdData' => empty($dataCollections) ? 0 : $this->dataCollectionFacade->countDataInDataCollections($dataCollections),
                'createdDataCollections' => empty($dataCollections) ? 0 : count($dataCollections),
                'isDataSaved' => $isDataSaved,
            ]
        );
    }
}
