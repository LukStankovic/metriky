<?php

namespace App\Controller\Front;

use App\Component\LayoutExport\LayoutExportFacade;
use App\Component\LayoutImport\LayoutImportFacade;
use App\Form\Front\LayoutImportFormType;
use App\Model\Layout\LayoutFacade;
use App\Model\Metric\MetricFacade;
use App\Model\User\UserFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class LayoutController extends AbstractController
{
    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;

    /**
     * @var \App\Model\User\UserFacade
     */
    private $userFacade;

    /**
     * @var \App\Component\LayoutExport\LayoutExportFacade
     */
    private $layoutExportFacade;

    /**
     * @var \App\Component\LayoutImport\LayoutImportFacade
     */
    private $layoutImportFacade;
    /**
     * @var \App\Model\Layout\LayoutFacade
     */
    private $layoutFacade;

    /**
     * @param \App\Component\LayoutExport\LayoutExportFacade $layoutExportFacade
     * @param \App\Component\LayoutImport\LayoutImportFacade $layoutImportFacade
     * @param \App\Model\Metric\MetricFacade $metricFacade
     * @param \App\Model\User\UserFacade $userFacade
     * @param \App\Model\Layout\LayoutFacade $layoutFacade
     */
    public function __construct(
        LayoutExportFacade $layoutExportFacade,
        LayoutImportFacade $layoutImportFacade,
        MetricFacade $metricFacade,
        UserFacade $userFacade,
        LayoutFacade $layoutFacade
    ) {
        $this->metricFacade = $metricFacade;
        $this->userFacade = $userFacade;
        $this->layoutExportFacade = $layoutExportFacade;
        $this->layoutImportFacade = $layoutImportFacade;
        $this->layoutFacade = $layoutFacade;
    }

    /**
     * @Route("/export-layout", name="export_layout")
     */
    public function export()
    {
        $layout = $this->layoutExportFacade->getExportedLayout();

        $response = new Response($layout);
        $response->headers->set('Content-Type', 'xml');

        return $response;
    }

    /**
     * @Route("/import-layout", name="import_layout")
     */
    public function import(Request $request)
    {
        $form = $this->createForm(LayoutImportFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedXmlFile */
            $uploadedXmlFile = $form['xml']->getData();

            if ($uploadedXmlFile) {
                $originalFilename = pathinfo($uploadedXmlFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate(
                    'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename
                );
                $newFilename = $safeFilename . '-' . uniqid() . '_' . $this->userFacade->getLoggedUser()->getId() . '.' . $uploadedXmlFile->guessExtension();

                try {
                    $uploadedXmlFile->move(
                        $this->getParameter('imported_layouts'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }

                $newFilePath = $this->getParameter('imported_layouts') . $newFilename;
                $this->layoutImportFacade->importLayout($newFilePath);

                return $this->redirectToRoute('dashboard');
            }
        }

        return $this->render('Front/layout_import/import.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/layout/ajax/remove", name="ajax_remove_position")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function remove(Request $request)
    {
        $layoutId = (int) $request->get('layoutId');
        $this->layoutFacade->removeByLayoutIdAndUser($layoutId, $this->getUser());

        return $this->forward('App\Controller\Front\DashboardController::index');
    }

    /**
     * @Route("/layout/ajax/update-positions", name="ajax_update_positions")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updatePositions(Request $request)
    {
        $layouts = $request->get('layouts');

        $foundLayouts = [];
        foreach ($layouts as $layout) {
            $layoutId = (int) $layout['layoutId'];
            $position = (int) $layout['position'];

            $foundLayout = $this->layoutFacade->getByLayoutId($layoutId, $this->getUser());

            if ($foundLayout !== null) {
                $foundLayout->setPosition($position);
                $foundLayouts[] = $foundLayout;
            }
        }
        $this->layoutFacade->updateAll();

        return $this->forward('App\Controller\Front\DashboardController::index');
    }
}
