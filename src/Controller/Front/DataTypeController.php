<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Form\Front\MetricDataTypeFormType;
use App\Model\Data\Type\DataType;
use App\Model\Data\Type\DataTypeFacade;
use App\Model\Metric\Metric;
use App\Model\Metric\MetricFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DataTypeController extends AbstractController
{
    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;
    /**
     * @var \App\Model\Data\Type\DataTypeFacade
     */
    private $dataTypeFacade;

    /**
     * @param \App\Model\Metric\MetricFacade $metricFacade
     * @param \App\Model\Data\Type\DataTypeFacade $dataTypeFacade
     */
    public function __construct(
        MetricFacade $metricFacade,
        DataTypeFacade $dataTypeFacade
    ) {
        $this->metricFacade = $metricFacade;
        $this->dataTypeFacade = $dataTypeFacade;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int $metricId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("metrika/typ-dat/{metricId}", name="metric_data_type")
     */
    public function newDataType(Request $request, int $metricId)
    {
        $metric = $this->metricFacade->getById($metricId);
        $dataType = new DataType();
        $dataType->setMetric($metric);

        $form = $this->createForm(MetricDataTypeFormType::class, $dataType);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $dataType->setName($form->get('name')->getData());
            $dataType->setType($form->get('type')->getData());
            $dataType->setDescription($form->get('description')->getData());
            $dataType->setMetric($metric);

            $this->dataTypeFacade->save($dataType);

            return $this->render('Front/metric/detail.html.twig', [
                    'metric' => $metric,
                ]
            );
        }

        return $this->render('Front/metric/data/metric_data_type.html.twig', [
                'metric' => $metric,
                'dataTypeForm' => $form->createView(),
            ]
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int $metricId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("metrika/{metricId}/typ-dat/{dataTypeId}", name="metric_data_type_edit")
     */
    public function dataTypeEdit(Request $request, int $metricId, int $dataTypeId)
    {
        $metric = $this->metricFacade->getById($metricId);
        $dataType = $this->dataTypeFacade->getById($dataTypeId);

        $form = $this->createForm(MetricDataTypeFormType::class, $dataType);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $dataType->setName($form->get('name')->getData());
            $dataType->setType($form->get('type')->getData());
            $dataType->setDescription($form->get('description')->getData());
            $dataType->setMetric($metric);

            $this->dataTypeFacade->save($dataType);

            return $this->render('Front/metric/detail.html.twig', [
                    'metric' => $metric,
                ]
            );
        }

        return $this->render('Front/metric/data/metric_data_type.html.twig', [
                'metric' => $metric,
                'dataTypeForm' => $form->createView(),
            ]
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int $metricId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("metrika/{metricId}/typ-dat/{dataTypeId}/remove", name="metric_data_type_remove")
     */
    public function dataTypeRemove(Request $request, int $metricId, int $dataTypeId)
    {
        $dataType = $this->dataTypeFacade->getById($dataTypeId);
        $this->dataTypeFacade->remove($dataType);

        return $this->redirectToRoute('metric_detail', ['metricId' => $metricId]);
    }
}
