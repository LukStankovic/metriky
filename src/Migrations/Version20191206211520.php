<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191206211520 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_collection ADD metric_id INT NOT NULL');
        $this->addSql('ALTER TABLE data_collection ADD CONSTRAINT FK_33785FEBA952D583 FOREIGN KEY (metric_id) REFERENCES metric (id)');
        $this->addSql('CREATE INDEX IDX_33785FEBA952D583 ON data_collection (metric_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_collection DROP FOREIGN KEY FK_33785FEBA952D583');
        $this->addSql('DROP INDEX IDX_33785FEBA952D583 ON data_collection');
        $this->addSql('ALTER TABLE data_collection DROP metric_id');
    }
}
