<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200313221603 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE layout (id INT AUTO_INCREMENT NOT NULL, metric_id INT DEFAULT NULL, user_id INT DEFAULT NULL, position INT NOT NULL, INDEX IDX_3A3A6BE2A952D583 (metric_id), INDEX IDX_3A3A6BE2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE layout ADD CONSTRAINT FK_3A3A6BE2A952D583 FOREIGN KEY (metric_id) REFERENCES metric (id)');
        $this->addSql('ALTER TABLE layout ADD CONSTRAINT FK_3A3A6BE2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
    }
}
