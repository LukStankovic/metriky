<?php

declare(strict_types=1);

namespace App\Form\Front;

use App\Model\Metric\Metric;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class AddMetricToLayoutFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('metric', EntityType::class, [
                'label' => 'Dostupné metriky',
                'class' => Metric::class,
                'choices' => $options['data'],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Přidat',
                'attr' => ['class' => 'btn btn--primary'],
            ]);
    }
}
