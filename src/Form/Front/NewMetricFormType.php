<?php

namespace App\Form\Front;

use App\Model\Graph\Graph;
use App\Model\Metric\Metric;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class NewMetricFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Název',
                'attr' => [
                    'placeholder' => 'Název',
                ],
                'constraints' => [new NotBlank(['message' => 'Vložte prosím název metriky'])],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Popis',
            ])
            ->add('graph', EntityType::class, [
                'class' => Graph::class,
                'choice_label' => 'name',
                'label' => 'Graf pro zobrazení',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Metric::class,
        ]);
    }
}
