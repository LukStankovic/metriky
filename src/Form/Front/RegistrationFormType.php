<?php

namespace App\Form\Front;

use App\Model\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'E-mail',
                'attr' => [
                    'placeholder' => 'E-mail',
                ],
                'constraints' => [new NotBlank(['message' => 'Vložte prosím e-mail'])],
            ])
            ->add('name', TextType::class, ['label' => 'Jméno', 'attr' => ['placeholder' => 'Jméno']])
            ->add('surname', TextType::class, ['label' => 'Příjmení', 'attr' => ['placeholder' => 'Příjmení']])
            ->add('plainPassword', PasswordType::class, [
                'mapped' => false,
                'label' => 'Heslo',
                'attr' => ['placeholder' => 'Heslo'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vložte prosím heslo',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Heslo musí mít alespoň {{ limit }} znaků',
                        'max' => 4096,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
