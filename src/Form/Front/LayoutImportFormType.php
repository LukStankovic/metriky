<?php

declare(strict_types=1);

namespace App\Form\Front;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class LayoutImportFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('xml', FileType::class, [
                'label' => 'XML se souborem pro import',
                'attr' => [
                    'placeholder' => 'XML',
                ],
                'constraints' => [
                    new NotBlank(['message' => 'Vložte prosím XML pro import']),
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'text/xml',
                            'application/xml',
                        ],
                        'mimeTypesMessage' => 'Vlože prosím XML',
                    ]),
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Nahrát',
                'attr' => ['class' => 'btn btn--primary'],
            ]);
    }
}
