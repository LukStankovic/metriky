<?php

declare(strict_types=1);

namespace App\Form\Front;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class CsvImportFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('csv', FileType::class, [
                'label' => 'CSV',
                'attr' => [
                    'placeholder' => 'CSV',
                ],
                'constraints' => [
                    new NotBlank(['message' => 'Vložte prosím CSV']),
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'text/plain',
                            'text/x-csv',
                            'application/csv',
                            'application/x-csv',
                        ],
                        'mimeTypesMessage' => 'Vlože prosím CSV',
                    ]),
                ],
            ]);
    }
}
