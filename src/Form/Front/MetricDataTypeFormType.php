<?php

namespace App\Form\Front;

use App\Model\Data\Type\DataType;
use App\Model\Data\Type\DataTypeFacade;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class MetricDataTypeFormType extends AbstractType
{
    /**
     * @var \App\Model\Data\Type\DataTypeFacade
     */
    private $dataTypeFacade;

    /**
     * @param \App\Model\Data\Type\DataTypeFacade $dataTypeFacade
     */
    public function __construct(DataTypeFacade $dataTypeFacade)
    {
        $this->dataTypeFacade = $dataTypeFacade;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Název sloupce',
                'constraints' => [new NotBlank(['message' => 'Vložte prosím název'])],
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Datový typ',
                'choices' => [
                    'Integer - celé číslo' => 'int',
                    'Double - desetinné číslo' => 'double',
                    'String (max 255 znaků)' => 'string',
                    'Datum' => 'date',
                    'Datum s časem' => 'datetime', // todo LS napsat zde tvar
                ],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Popis',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DataType::class,
        ]);
    }
}
