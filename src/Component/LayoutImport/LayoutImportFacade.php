<?php

declare(strict_types=1);

namespace App\Component\LayoutImport;

use App\Model\Layout\Layout;
use App\Model\Layout\LayoutFacade;
use App\Model\Metric\MetricFacade;
use App\Model\User\UserFacade;
use DOMDocument;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use XMLReader;

class LayoutImportFacade
{
    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;

    /**
     * @var \App\Model\User\UserFacade
     */
    private $userFacade;

    /**
     * @var \App\Model\Layout\LayoutFacade
     */
    private $layoutFacade;

    /**
     * @var \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var \App\Model\User\User
     */
    private $user;

    /**
     * @param \App\Model\Metric\MetricFacade $metricFacade
     * @param \App\Model\User\UserFacade $userFacade
     * @param \App\Model\Layout\LayoutFacade $layoutFacade
     * @param \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface $parameterBag
     */
    public function __construct(
        MetricFacade $metricFacade,
        UserFacade $userFacade,
        LayoutFacade $layoutFacade,
        ParameterBagInterface $parameterBag
    ) {
        $this->metricFacade = $metricFacade;
        $this->userFacade = $userFacade;
        $this->layoutFacade = $layoutFacade;
        $this->parameterBag = $parameterBag;

        $this->user = $this->userFacade->getLoggedUser();
    }

    public function importLayout(string $xmlFilePath)
    {
        $reader = new XMLReader();
        $doc = new DOMDocument;

        if (!$reader->open($xmlFilePath)) {
            die('Failed to open ' . $xmlFilePath);
        }

        $layouts = [];

        while ($reader->read()) {
            if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'metric') {
                $node = simplexml_import_dom($doc->importNode($reader->expand(), true));
                $metricId = (int) $node->id;
                $position = (int) $node->position;

                $metric = $this->metricFacade->getById($metricId);

                if ($metric !== null) {
                    $layout = new Layout();
                    $layout->setMetric($metric);
                    $layout->setUser($this->user);
                    $layout->setPosition($position);
                    $layouts[] = $layout;
                }
            }
        }
        $reader->close();

        $this->layoutFacade->removeAllUserLayouts($this->user);
        $this->layoutFacade->saveAll($layouts);
    }
}
