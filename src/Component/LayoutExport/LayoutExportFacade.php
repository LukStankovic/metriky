<?php

declare(strict_types=1);

namespace App\Component\LayoutExport;

use App\Model\Layout\LayoutFacade;
use App\Model\Metric\MetricFacade;
use App\Model\User\UserFacade;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use XMLWriter;

class LayoutExportFacade
{
    /**
     * @var \App\Model\Metric\MetricFacade
     */
    private $metricFacade;

    /**
     * @var \App\Model\User\UserFacade
     */
    private $userFacade;
    /**
     * @var \App\Model\Layout\LayoutFacade
     */
    private $layoutFacade;
    /**
     * @var \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @param \App\Model\Metric\MetricFacade $metricFacade
     * @param \App\Model\User\UserFacade $userFacade
     * @param \App\Model\Layout\LayoutFacade $layoutFacade
     * @param \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface $parameterBag
     */
    public function __construct(
        MetricFacade $metricFacade,
        UserFacade $userFacade,
        LayoutFacade $layoutFacade,
        ParameterBagInterface $parameterBag
    ) {
        $this->metricFacade = $metricFacade;
        $this->userFacade = $userFacade;
        $this->layoutFacade = $layoutFacade;
        $this->parameterBag = $parameterBag;
    }

    public function getExportedLayout()
    {
        $user = $this->userFacade->getLoggedUser();
        $layouts = $this->layoutFacade->findByUser($user);

        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->setIndent(true);
        $xmlWriter->startDocument('1.0', 'UTF-8');

        $xmlWriter->startElement('metrics');
        foreach ($layouts as $layout) {
            $xmlWriter->startElement('metric');
            $xmlWriter->writeElement('id', (string) $layout->getMetric()->getId());
            $xmlWriter->writeElement('position', (string) $layout->getMetric()->getId());

            $xmlWriter->endElement();
        }
        $xmlWriter->endElement();
        $xmlWriter->endDocument();

        return $xmlWriter->outputMemory();
    }
}
