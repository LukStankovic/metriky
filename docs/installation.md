# Instalace
- Naklonovat si repozitář
- Přesunout se do složky s projektem

## Vytvoření databáze a nainstalování balíčků
- Spustit příkaz `composer install`
- Vytvořit si databázi
- Vytvořit soubor `.env.local` ve tvaru:
```
APP_ENV=dev
DATABASE_URL=mysql://user:password@db_server/database_name
```

## Vytvoření tabulek databáze
- spustit migrace: `composer db-migrations` nebo `php bin/console doctrine:migrations:migrate`

## Demo data
-  Vytvoření demo dat: `composer data-fixtures-load`
    - Vytvoří 2 uživatele:
        - `admin@vsb.cz` s heslem `admin1234` s administrátorskými právy
        - `user@vsb.cz` s heslem `user1234` s uživatelskými právy
    - Vytvoří testovací metriky
    
## Spuštění aplikace
- Příkaz: `symfony server:start`
- Není třeba spouštět `Apache` v například aplikaci `xampp` 