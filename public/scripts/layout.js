$(document).ready(function () {
    $(document).on('click', '.js-layout-remove', function () {
        const layoutId = $(this).data('id');

        let url = "/layout/ajax/remove";
        if (window.location.hostname === "vsrvfeia0h-93.vsb.cz") {
            url = "/sta-cia" + url;
        }

        $.ajax({
            url: url,
            type: 'POST',
            data: {
                layoutId: layoutId
            },
            success: function (data) {
                let layoutMetrics = $(data).find('.js-main-body-block');
                $('.js-main-body').html(layoutMetrics);
            },
            error: function () {
                alert("Blok nelze odstranit.");
            }
        });
    });

});